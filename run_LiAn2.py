import numpy as np
import skimage

from point.Point import Point
import matplotlib.pyplot as plt
from LiAn.LiAn import LiAn
from LiAn.LiAnUtils import LiAnUtils
from WaveAlgo import WaveAlgo

bmap1 = np.zeros((50, 50))
bmap1[0:16, 5:21] = 1
bmap1[0:9, 23:36] = 1
bmap1[0:2, 36:39] = 1
bmap1[15:21, 26:35] = 1



bmap2 = np.zeros((100, 100))
bmap2[30:50, 30:50] = 1
# points = LiAnUtils.get_circle_cuccessors(Point(50, 50), 20)
# for p in points:
#     bmap2[p.y][p.x] = 1



plt.imshow(bmap2)
plt.show()

print("line", LiAnUtils.border_on_a_line(bmap2, Point(0, 0),Point(90, 90)))

angle = 30
start = Point(10, 10, 0)
finish = Point(90, 90)
plt_start_point = plt.Circle((start.x, start.y), 1, color='y')
plt_finish_finish = plt.Circle((finish.x, finish.y), 1, color='r')

# wave_color = (0, 255, 0)
# wave = WaveAlgo()
# wave.search(binary_map, start, finish)
# wave_path = wave.find_path(start, finish)
# for p in wave_path:
#     image[p.y, p.x] = wave_color

# plt.imshow(bmap2)
# plt.show()
lian = LiAn(bmap2)
path = lian.search(start, finish, 10, angle)

visited = lian.closed
visited_map = np.zeros_like(bmap2)

for v in visited:
    bmap2[v.y, v.x] = 3




# plt.imshow(bmap2)
# plt.show()
path_on_map = LiAn.draw_path(path, bmap2, 5)
plt.imshow(path_on_map)
plt.show()

# print(lian.closed_dict)
for p in lian.closed_dict:
    # print(point_arrays)
    # for p in point_arrays:
    path_on_map[p[0]][p[1]] = 0.5
    # print(p)
plt.imshow(path_on_map)
plt.show()


# fig, ax = plt.subplots()
# ax.imshow(binary_map)
# ax.add_patch(plt_start_point)
# ax.add_patch(plt_finish_finish)
# plt.title("original map")
# plt.show()

# m1 = LiAn.draw_path(path, binary_map)
# fig1, ax1 = plt.subplots()
# ax1.imshow(m1)
# ax1.add_patch(plt.Circle((start.x, start.y), 5, color='y'))
# ax1.add_patch(plt.Circle((finish.x, finish.y), 5, color='r'))
# plt.title(f"angle: {angle}")
# plt.show()
# plt.title(f"path on map. LiAn angle: {angle}")




