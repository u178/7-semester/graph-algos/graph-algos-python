import math
from queue import PriorityQueue
from typing import Union, List
import numpy as np

Num = Union[int, float]


def djkstra(nodes: int, first_node: int, matrix: List[List[Num]]) -> (List[Num], List[int]):
    unvisited = set(_i for _i in range(nodes))
    weights = [math.inf for _ in range(nodes)]
    previous_node = [None for _ in range(nodes)]
    weights[first_node] = 0

    q = PriorityQueue(maxsize=1000)
    q.put((0, first_node))

    while not q.empty():
        weight, node = q.get()
        if node in unvisited:
            unvisited.remove(node)
            for j in range(n):
                if j != node and j in unvisited and matrix[node][j] >= 0:
                    if weight + matrix[node][j] < weights[j]:
                        weights[j] = weight + matrix[node][j]
                        previous_node[j] = node

                    q.put((weight + matrix[node][j], j))
        else:
            continue
    return weights, previous_node


def get_path(node: int, previous_node: List[int]) -> List[int]:
    path = [node]
    cur_node = node
    while previous_node[cur_node] is not None:
        path.append(previous_node[cur_node])
        cur_node = previous_node[cur_node]
    return path


if __name__ == "__main__":
    # adjacency matrix
    # # number of nodes
    # n = int(input())
    # matrix = []
    # for i in range(n):
    #     matrix.append(list(map(int, input().split())))
    # first_node = int(input())


    # incidence matrix
    n = int(input())
    matrix = [[-1 for _i in range(n)] for _ in range(n)]
    edges = int(input())
    for _ in range(edges):
        s, f, w = list(map(int, input().split()))
        matrix[s-1][f-1] = w
    first_node = int(input()) -1
    last_node = int(input()) -1

    # example of incidence matrix
    """
    9
    14
    1 2 10
    1 4 8
    1 3 6
    2 7 11
    2 4 5
    3 5 3
    4 7 12
    4 6 7
    4 5 5
    5 9 12
    6 8 8
    6 9 10
    7 8 6
    8 9 15
    1
    9
    """

    # example of adjacency matrix input
    """
    2
    0 1
    2 0
    0
    """

    """
    2
    0 -1
    -1 0
    0
    """

    """
    6
    0 -1 -1 -1 -1 10
    -1 0 -1 11 -1 2
    -1 -1 0 5 -1 4
    -1 11 5 0 7 -1
    -1 -1 -1 7 0 -1
    10 2 4 -1 -1 0
    0
    """


    weights, previous_node = djkstra(n, first_node, matrix)


    print("path:", list(reversed(np.array(get_path(last_node, previous_node))+1)))
    previous_node[first_node] = -1

    print(f"weights: {weights}\nprevious nodes: {np.array(previous_node) + 1}")
