from point.Point import Point
from queue import PriorityQueue
import numpy as np
import matplotlib.pyplot as plt
import math

adjacency4 = [(-1, 0), (1, 0), (0, -1), (0, 1)]
adjacency8 = [(-1, 0), (1, 0), (0, -1), (0, 1), (-1, -1), (1, 1), (-1, 1), (1, -1)]

selected_adjacency = adjacency8
borders = np.array((255, 0, 0))


def _calc_edge_weight(node1: Point, node2: Point):
    # print(node1, node2)
    return Point.calc_euclidean_distance(node1, node2)


class WaveAlgo:
    def __init__(self):
        pass

    def search(self, map_array: np.ndarray, start: Point, finish: Point, show_map=False):
        self.map_array = map_array
        # print(self.map_array.shape)
        # plt.imshow(map_array)
        # plt.show()
        self.distances_array = np.full(map_array.shape, 10000000)
        self.visited = np.full(map_array.shape, False)
        height, width = map_array.shape
        q = PriorityQueue()
        start.weight = 0
        self.visited[start.y, start.x] = True
        q.put((0, start))
        iters = 0

        while not q.empty():
            iters += 1

            weight, cur_point = q.get()
            self.distances_array[cur_point.y, cur_point.x] = weight
            #print(iters)
            # print(cur_point)
            if cur_point.x == finish.x and cur_point.y == finish.y:
                print(f"Finish was found. Required iterations: {iters}")
                break

            for a in selected_adjacency:
                new_point = Point(cur_point.x + a[0],
                                  cur_point.y + a[1])
                if 0 <= new_point.x < self.map_array.shape[1] and 0 <= new_point.y < self.map_array.shape[0] and \
                        not self.visited[new_point.y][new_point.x] and self.map_array[new_point.y][new_point.x] != 1:
                    self.visited[new_point.y][new_point.x] = True
                    new_weight = _calc_edge_weight(cur_point, new_point)
                    q.put((weight + new_weight, new_point))

            if not iters % 200000 and show_map:
                print(iters)
                plt.imshow(self.distances_array)
                plt.title(f"iterations: {iters}")
                plt.show()

        if show_map:
            print(f"iters: {iters}")
            plt.imshow(self.distances_array)
            plt.title(f"iterations: {iters}")
            plt.show()

    def find_path(self, start: Point, finish: Point, show_map=False):
        m = np.zeros_like(self.map_array)
        path = []
        cur_point = Point(finish.x, finish.y)
        path.append(cur_point)
        iters = 0
        while cur_point != start:
            m[cur_point.y, cur_point.x] = 1
            iters += 1

            possible_points = []
            weights = []

            for a in selected_adjacency:
                new_point = Point(cur_point.x + a[0], cur_point.y + a[1])
                possible_points.append(new_point)
                weights.append(self.distances_array[new_point.y, new_point.x])
            x = np.argmin(weights)
            new_point = possible_points[x]
            path.append(new_point)
            cur_point = new_point

            if not iters % 100000 and show_map:
                print(iters)
                plt.imshow(m)
                plt.title(f"path searching {iters}")
                plt.show()
        return path



