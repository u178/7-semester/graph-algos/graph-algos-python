import numpy as np

from point.Point import Point
import math
import matplotlib.pyplot as plt


class LiAnUtils:
    @staticmethod
    def get_circle_cuccessors(center: Point, r: float):
        points = []
        x = r
        y = 0
        points.append(Point(center.x + r, center.y))
        points.append(Point(center.x, center.y + r))
        points.append(Point(center.x, center.y - r))
        points.append(Point(center.x - r, center.y))

        P = 1 - r
        while x > y:
            y += 1
            if P <= 0:
                P = P + 2 * y + 1
            else:
                x -= 1
                P = P + 2 * y - 2 * x + 1
            if x < y:
                return points

            points.append(Point(x + center.x, y + center.y))
            points.append(Point(-x + center.x, y + center.y))
            points.append(Point(x + center.x, -y + center.y))
            points.append(Point(-x + center.x, -y + center.y))

            if x != y:
                points.append(Point(y + center.x, x + center.y))
                points.append(Point(-y + center.x, x + center.y))
                points.append(Point(y + center.x, -x + center.y))
                points.append(Point(-y + center.x, -x + center.y))

        for p in points:
            p.distance = center.distance + p.distance_to(center)
            p.previous_point = p

        return points

    @staticmethod
    def get_line_points(start: Point, finish: Point):
        x1 = start.x
        x2 = finish.x
        y1 = start.y
        y2 = finish.y
        points = []
        issteep = abs(y2 - y1) > abs(x2 - x1)
        if issteep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
        rev = False
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
            rev = True
        deltax = x2 - x1
        deltay = abs(y2 - y1)
        error = int(deltax / 2)
        y = y1
        ystep = None
        if y1 < y2:
            ystep = 1
        else:
            ystep = -1
        for x in range(x1, x2 + 1):
            if issteep:
                points.append(Point(y, x))
            else:
                points.append(Point(x, y))
            error -= deltay
            if error < 0:
                y += ystep
                error += deltax
        # Reverse the list if the coordinates were reversed
        if rev:
            points.reverse()
        return points

    @staticmethod
    def border_on_a_line(m: np.ndarray, start: Point, finish: Point):

        line_of_sight = LiAnUtils.get_line_points(start, finish)
        for p in line_of_sight:
            if m[p.y, p.x] == 1:
                return True
        return False

    @staticmethod
    def calc_angle(node1, node2, node3, node4):
        s1 = LiAnUtils._slope(node1.x, node1.y, node2.x, node2.y)
        s2 = LiAnUtils._slope(node3.x, node3.y, node4.x, node4.y)
        return math.degrees(math.atan((s2 - s1) / (1 + (s2 * s1))))

    @staticmethod
    def _slope(x1, y1, x2, y2):  # Line slope given two points:
        if x1 == x2:
            return 0
        return (y2 - y1) / (x2 - x1)

    @staticmethod
    def angle(node1, node2, node3, node4):
        line1_v = (node2.x - node1.x, node2.y - node1.y)
        line2_v = (node4.x - node3.x, node4.y - node3.y)
        line1_d = math.sqrt(line1_v[0]**2 + line1_v[1]**2)
        line2_d = math.sqrt(line2_v[0]**2 + line2_v[1]**2)
        # print(node1)
        # print(node2)
        # print(node3)
        # print(node4)
        cos = (line1_v[0] * line2_v[0] + line1_v[1] * line2_v[1]) / (line1_d * line2_d)
        if cos > 1.:
            cos = 1.
        elif cos < -1.:
            cos = -1
        return math.acos(cos) * 180 / math.pi


if __name__ == "__main__":
    m = np.zeros((100, 100))
    p = Point(50, 50)
    points = LiAnUtils.get_circle_cuccessors(p, 7)

    m[p.y, p.x] = 3
    for p1 in points:
        m[p1.y, p1.x] = 1

    p1 = Point(20, 4)
    p2 = Point(40, 4)
    p3 = Point(0, 20)
    p4 = Point(0, 40)

    # print(LiAnUtils.calc_angle(p2, p1, p3, p4))

    points = LiAnUtils.get_line_points(p1, p2)
    print(points)
    for p1 in points:
        m[p1.y, p1.x] = 5

    plt.imshow(m)
    plt.show()

    p1 = Point(20, 20)
    p2 = Point(40, 20)
    p3 = Point(60, 25)
    p4 = Point(60, 15)
    p5 = Point(40, 40)
    print(LiAnUtils.calc_angle(p1, p2, p2, p3))
    print(LiAnUtils.calc_angle(p1, p2, p2, p4))
    print(LiAnUtils.calc_angle(p2, p1, p2, p4))

    print(LiAnUtils.angle(p1, p2, p2, p3))
    print(LiAnUtils.angle(p1, p2, p2, p4))
    print(LiAnUtils.angle(p2, p1, p2, p4))

    print(LiAnUtils.angle(p1, p2, p2, p5))

    map = np.zeros((10, 10))
    map[0:9, 5] = 1
    start = Point(2, 2)
    finish = Point(8, 7)
    print(LiAnUtils.border_on_a_line(map, start, finish))
    print(LiAnUtils.border_on_a_line(map, Point(1, 1), Point(1, 9)))

