from point.Point import Point
from queue import PriorityQueue
import numpy as np
import matplotlib.pyplot as plt
from LiAn.LiAnUtils.LiAnUtils import LiAnUtils


class LiAn:
    def __init__(self, map: np.ndarray):
        self.finish: Point = None
        self.iters = None
        self.map = map
        self.q = PriorityQueue()
        self.closed = list()
        self.closed_dict = {}
        self.closed_set = set()
        self.distances = np.full(map.shape, np.inf)
        self.path = []

    def search(self, start: Point, finish: Point, distance: float, angle: float, draw_visited=True):
        self.path = []
        self.closed.clear()
        self.closed_dict = {}
        self.distances = np.full(self.map.shape, np.inf)
        self.distances[start.y, start.x] = 0
        self.iters = 0

        self.start = start
        self.start.distance = 0
        self.start.previous_point = Point(self.start.x, self.start.y)
        self.finish = finish

        self.q = PriorityQueue()
        self.q.put((0, self.start))
        while not self.q.empty():
            self.iters += 1

            weight, cur_point = self.q.get()

            if not self.iters % 10000:
                # print(self.iters, self.q.qsize(), len(self.closed))
                print("closed dict", self.iters, self.q.qsize(), len(self.closed_set))
                print(cur_point, weight)
                # m = np.zeros_like(self.map)
                # for p in self.closed:
                #     m[p.y, p.x] = 1
                # plt.imshow(m)
                # plt.title(f"iters: {self.iters}")
                # plt.show()
            if cur_point == finish:
                print("path was found!")
                self.get_path(cur_point)
                # print(self.path)
                return self.path
            if cur_point.previous_point is None:
                print("previous point is None")
                # print(cur_point)
            if (cur_point.x, cur_point.y, cur_point.previous_point.x, cur_point.previous_point.y) in self.closed_set:
                continue

            # self.closed.append(cur_point)
            # if (cur_point.x, cur_point.y) in self.closed_dict:
            #     self.closed_dict[(cur_point.x, cur_point.y)].append(cur_point)
            # else:
            #     self.closed_dict[(cur_point.x, cur_point.y)] = [cur_point]

            # if (cur_point.x, cur_point.y, cur_point.previous_point.x, cur_point.previous_point.y) not in self.closed_dict:
            #     self.closed_dict[(cur_point.x, cur_point.y, cur_point.previous_point.x, cur_point.previous_point.y)] = True
            if (cur_point.x, cur_point.y, cur_point.previous_point.x, cur_point.previous_point.y) not in self.closed_set:
                self.closed_set.add((cur_point.x, cur_point.y, cur_point.previous_point.x, cur_point.previous_point.y))

            # print(self.start)
            self._expand(cur_point, distance, angle)
        print("path wasn't found")
        return False

    def _expand(self, cur_point: Point, distance: float, angle: float):
        successors = LiAnUtils.get_circle_cuccessors(cur_point, distance)
        if self._calc_distance(cur_point, self.finish) <= distance:
            successors.append(self.finish)

        for point in successors:
            if point.y < 0 or point.x < 0 or \
                    point.y >= self.map.shape[0] or point.x >= self.map.shape[1]:
                continue

            if self.map[point.y][point.x] == 1:
                continue
            # print(f"cur point: {cur_point}")
            if cur_point != self.start and \
                    LiAnUtils.angle(cur_point.previous_point, cur_point, cur_point, point) > angle:
                continue

            continue_flag = False
            if (point.x, point.y, cur_point.x, cur_point.y) in self.closed_set:
                continue
            #     for closed_point in self.closed_dict[(point.x, point.y)]:
            #         # if point is None or closed_point is None:
            #         #     continue_flag = True
            #         #     break
            #         if point.previous_point == closed_point.previous_point:
            #             continue_flag = True
            #             break
            # if continue_flag:
            #     continue

            if LiAnUtils.border_on_a_line(self.map, cur_point, point):
                # print("skip")
                continue

            point.previous_point = cur_point
            point.distance = cur_point.distance + self._calc_distance(cur_point, point)

            # print(point)
            self.q.put((self._calc_distance(point, self.finish) + point.distance, point))
            # self.q.put((self._calc_distance(point, self.finish), point))

    def _calc_distance(self, node1: Point, node2: Point):
        # print(node1, node2)
        return Point.calc_euclidean_distance(node1, node2)

    def get_path(self, point: Point):
        self.path.append(Point(point.x, point.y))
        if point != self.start:
            self.get_path(point.previous_point)

    @staticmethod
    def draw_path(path: list[Point], map: np.ndarray, color=5) -> np.ndarray:
        m = map.copy()
        for i in range(len(path) - 1):
            points = LiAnUtils.get_line_points(path[i], path[i + 1])
            for p1 in points:
                m[p1.y, p1.x] = color
        return m
