import skimage.io
import matplotlib.pyplot as plt
import numpy as np
from tqdm import trange
from point.Point import Point
from queue import PriorityQueue


adjacency4 = [(-1, 0), (1, 0), (0, -1), (0, 1)]
adjacency8 = [(-1, 0), (1, 0), (0, -1), (0, 1), (-1, -1), (1, 1), (-1, 1), (1, -1)]

selected_adjacency = adjacency8
colors = np.array(((255, 201, 14), (237, 28, 36), (255, 0, 0)))

borders = np.array((255, 0, 0))


def fill_building(map: np.ndarray, x_start: int, y_start: int, is_entrance=False):
    to_go = [(x_start, y_start)]
    # map[x, y, 0] = 255
    while to_go:
        x, y = to_go.pop()
        if not np.any(map[x, y]):
            map[x, y, 0] = 255
            for a in selected_adjacency:
                if 0 <= x + a[0] < map.shape[0] and 0 <= y + a[1] < map.shape[1]:
                    if not np.any(map[x + a[0], y + a[1]]):
                        to_go.append((x + a[0], y + a[1]))


image = skimage.io.imread(fname="data/karta-01.bmp")
fig, ax = plt.subplots()
# plt.imshow(image)
# plt.show()

array = np.array(image)

print(array.shape)

buildings = 0

for i in trange(0, array.shape[0]):
    for j in range(0, array.shape[1]):
        if not np.any(array[i, j]):
            buildings += 1
            fill_building(array, i, j, True)
        elif np.any(array[i, j] - colors[0]) and np.any(array[i, j] - colors[1]) and np.any(array[i, j] - colors[2]):
            array[i, j, 0] = 255
            array[i, j, 1] = 255
            array[i, j, 2] = 255


visited = [[False for __ in range(array.shape[1])] for _ in range(array.shape[0])]


# def distance(point1: Point, point2: Point)

print(buildings)

# plt.subplot(121)
# plt.imshow(image)
# plt.subplot(122)
# plt.imshow(array)
# plt.show()


