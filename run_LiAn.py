import numpy as np
from point.Point import Point

import matplotlib.pyplot as plt
from LiAn.LiAn import LiAn

with open('data/binary_map.npy', 'rb') as f:
    binary_map = np.load(f)

angle = 90
start = Point(162, 307, 0)
finish = Point(1287, 690)
plt_start_point = plt.Circle((start.x, start.y), 5, color='y')
plt_finish_finish = plt.Circle((finish.x, finish.y), 5, color='r')

fig, ax = plt.subplots()
ax.imshow(binary_map)
ax.add_patch(plt_start_point)
ax.add_patch(plt_finish_finish)
plt.title("original map")
plt.show()


lian = LiAn(binary_map)
path = lian.search(start, finish, 10, angle)

m1 = LiAn.draw_path(path, binary_map)

fig1, ax1 = plt.subplots()
ax1.imshow(m1)
ax1.add_patch(plt.Circle((start.x, start.y), 5, color='y'))
ax1.add_patch(plt.Circle((finish.x, finish.y), 5, color='r'))
plt.title(f"angle: {angle}")
plt.show()
plt.title(f"path on map. LiAn angle: {angle}")
