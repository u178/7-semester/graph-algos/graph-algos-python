from typing import Union, List
import numpy as np
from tqdm import trange
from loguru import logger

Num = Union[int, float]


class Ant:
    tabu_list: list
    history: list
    path_length: float

    def __init__(self):
        self.tabu_list = list()
        self.history = []
        self.path_length = 0


class AntColonyError(Exception):
    pass


class AntColony:
    n: int = 0  # number of nodes
    first_node: int = 0
    weight_matrix: np.ndarray
    pheromone_matrix: np.ndarray
    ant_pheromone_modifier: float  # pheromone level modifier for edge weight
    weight_modifier: float  # edge weight modifier for edge weight
    pheromone_expiry: float  # pheromone expiry per iteration (pheromone *=(1 - pheromone_expiry)).
    # belongs to (0, 1)
    ant_pheromone: float  # pheromone ant leaves behind him.

    def __init__(self, ant_pheromone_modifier=1., weight_modifier=-1., pheromone_expiry=0.66, ant_pheromone=8.):
        self.ant_pheromone_modifier = ant_pheromone_modifier
        self.weight_modifier = weight_modifier
        if 0 >= pheromone_expiry >= 1:
            raise AntColonyError("pheromone_expiry can only belongs from (0 to 1)")
        self.pheromone_expiry = pheromone_expiry
        if ant_pheromone <= 0:
            raise AntColonyError("ant_pheromone should be greater than 0")
        self.ant_pheromone = ant_pheromone

    def read_adjacency_matrix_from_console(self):
        # adjacency matrix
        # number of nodes
        self.n = int(input())
        weight_matrix = []
        for i in range(self.n):
            weight_matrix.append(list(map(int, input().split())))
        self.weight_matrix = np.array(weight_matrix, dtype=float)
        self.pheromone_matrix = np.ones_like(self.weight_matrix, dtype=float)
        logger.info(f"adjacency_matrix from console\n{self.weight_matrix}")

    def read_adjacency_matrix_from_file(self, filename: str):
        f = open(filename, "r")
        self.n = int(f.readline())
        weight_matrix = []
        for i in range(self.n):
            weight_matrix.append(list(map(int, f.readline().split())))
        self.weight_matrix = np.array(weight_matrix, dtype=float)
        self.pheromone_matrix = np.ones_like(self.weight_matrix, dtype=float)
        logger.info(f"adjacency_matrix from file\n{self.weight_matrix}")

    def search(self, first_node: int, iterations: int = 1000, ants_per_tick: int = 100, end_node: int = None):
        # global iterations loop
        for _ in trange(iterations):
            new_pheromones = np.zeros_like(self.pheromone_matrix, dtype=float)  # set new_pheromones matrix to zero

            for __ in range(ants_per_tick):
                ant = Ant()
                ant.tabu_list.append(first_node)
                cur_node = first_node

                # one iteration of an ant
                while True:
                    possible_nodes = []
                    node_weights = []
                    for k in range(self.n):  # choose nodes where ant can go
                        if k != cur_node and self.weight_matrix[cur_node][k] > 0 and k not in ant.tabu_list:
                            edge_weight = self._calc_edge_weight(cur_node, k)
                            possible_nodes.append(k)
                            node_weights.append(edge_weight)

                    if not possible_nodes:  # if we ant is blocked
                        break
                    # choose node to go
                    probability = np.array(node_weights)
                    probability /= probability.sum()
                    node_to_go = np.random.choice(a=possible_nodes, p=probability)  # select a node from possible_nodes

                    # update weights and tabu_list
                    ant.path_length += self.weight_matrix[cur_node][node_to_go]
                    ant.tabu_list.append(node_to_go)
                    cur_node = node_to_go

                    # if we go to the exact node
                    if end_node is not None and cur_node == end_node:
                        break

                # update pheromone level
                if len(ant.tabu_list) > 0:
                    for k in range(len(ant.tabu_list) - 1):
                        new_pheromones[ant.tabu_list[k]][ant.tabu_list[k+1]] += (self.ant_pheromone / ant.path_length)

            self.pheromone_matrix *= (1 - self.pheromone_expiry)
            self.pheromone_matrix += new_pheromones

    def _calc_edge_weight(self, node1: int, node2: int):
        return (self.pheromone_matrix[node1][node2]**self.ant_pheromone_modifier) \
               * (self.weight_matrix[node1][node2]**self.weight_modifier)

    def get_the_most_pheromoned_path(self, first_node: int, threshold: float = 0.1):
        length = 0
        path = [first_node]
        cur_node = first_node
        matrix = self.pheromone_matrix.copy()

        while True:
            next_node = np.argmax(matrix[cur_node])
            if matrix[cur_node][next_node] <= threshold or self.weight_matrix[cur_node][next_node] < 0:
                return path, length

            if next_node in path:
                matrix[cur_node][next_node] = 0
            else:
                path.append(next_node)
                length += self.weight_matrix[cur_node][next_node]
                cur_node = next_node


# ToDo move to a superpermutation package
def create_permutation_matrix_items(permutation_items: List[Num]):
    # permutation_matrix_items = []
    # ToDo
    pass


def create_super_permutations_matrix(permutation_matrix_items: List[str]):
    # n = len(permutation_matrix_items)
    # permutation_matrix = [[0 for _i in range(n)] for _ in range(n)]
    # ToDo
    pass


if __name__ == "__main__":
    # np.random.seed(83)
    print(np.random.random())
    logger.add("ant.log")
    first_node = 0
    ac = AntColony(ant_pheromone_modifier=1.,
                   weight_modifier=-1.,
                   pheromone_expiry=0.33,
                   ant_pheromone=20.)

    ac.read_adjacency_matrix_from_file("data/adjacency_matrix_3.txt")

    ac.search(first_node=first_node,
              iterations=1000,
              ants_per_tick=10,
              end_node=8)
    print("pheromones:")
    print(np.round(ac.pheromone_matrix, 3))
    path, length = ac.get_the_most_pheromoned_path(first_node)
    print(f"path: {' -> '.join(map(str, np.array(path)+1))}")
    print(f"path length: {length}")

